# Overview

This is the easiest solution to use gulp on wordpress.
This support SASS and JS compile and minify + live reload.

### Installation

Make sure  [Node.js](https://nodejs.org/) is installed.

If you dont have gulp installed, the first thing you need to do is install gulp. Use the following command from terminal to do so.
```sh
$ npm install -g gulp 
```


Clone this repo to the root folder of your project.

```sh
$ git clone git@bitbucket.org:josejohnandrew-bbt/wp-gulp.git
```




Once the files are downloaded, run the install

```sh
$ npm install 
```

You might wanna create a bower.json file so you can load your site's dependencies there.

```sh
$ bower init 
```

I usually install [bootstrap](https://getbootstrap.com/) via bower
```sh
$ bower install bootstrap#v4.0.0-beta --save 
```

### Usage

After installing everything, just run the `gulp` code to your terminal, then viola!!!
Just make sure that you're on your project directory.
```sh
$ gulp 
```