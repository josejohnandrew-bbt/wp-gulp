//Require dependencies
var gulp = require('gulp');
var sass = require('gulp-sass');

//SCSS
var minifycss           = require('gulp-clean-css'); // Minifies CSS files.
var mmq                 = require('gulp-merge-media-queries'); // Combine matching media queries into one media query definition.
var rename              = require('gulp-rename'); // Renames files E.g. style.css -> style.min.css
var SassLocation        = './wp-content/themes/[THEMENAME]/assets/scss/*.scss';
var SassToCssLocation   = './wp-content/themes/[THEMENAME]/assets/css/';

var JSLocation          = './wp-content/themes/[THEMENAME]/assets/js/custom/*.js';
var JsDestination       = './wp-content/themes/[THEMENAME]/assets/js/';

var ImgLocation         = './web/app/themes/BBTFramework/assets/images/source/*';
var ImgDestination      = './web/app/themes/BBTFramework/assets/images/';


//bower
var concat = require('gulp-concat');
var gulpFilter = require('gulp-filter');
var mainBowerFiles = require('main-bower-files');
var uglify = require('gulp-uglify');

//Browser reload
var bower_location = './bower.json';
var browserSync  = require('browser-sync').create(); // Reloads browser and injects CSS. Time-saving synchronised browser testing.
var reload       = browserSync.reload; // For manual browser reload.

//Project variables
var project                 = 'Project name'; // Project Name.
var projectURL              = 'localhost/Project name/'; // Local project URL of your already running WordPress site. Could be something like local.dev or localhost:8888.
var projectPHPWatchFiles    = './**/*.php'; // Path to all PHP files.

//Watch Variables
var watchJS   = JSLocation;
var watchSass = SassLocation;

//Autobrowser reload
gulp.task( 'browser-sync', function() {
  browserSync.init( {
    // For more options
    // @link http://www.browsersync.io/docs/options/
    // Project URL.
    proxy: projectURL,
    // `true` Automatically open the browser with BrowserSync live server.
    // `false` Stop the browser from automatically opening.
    open: true,
    // Inject CSS changes.
    // Commnet it to reload browser for every CSS change.
    injectChanges: true,
    // Use a specific port (instead of the one auto-detected by Browsersync).
    // port: 7000,
  } );
});

//Minify sass files
gulp.task('styles', function() {
    gulp.src(SassLocation)
    	.pipe(sass({outputStyle: 'compressed'}))
        //.pipe(sass().on('error', sass.logError))
        .pipe( browserSync.stream() ) // Reloads style.css if that is enqueued.
        .pipe( rename( { suffix: '.min' } ) )
        .pipe( minifycss())
    	.pipe(gulp.dest( SassToCssLocation))
        .on('error', console.error.bind(console))
        ;
});

//Minify Js
gulp.task('scripts', function() {
    return gulp.src(JSLocation)
        .pipe(concat('scripts.js'))
        .pipe(uglify())
        .pipe(gulp.dest(JsDestination));
});

//optimize images
gulp.task('optimize-image', () =>
    gulp.src(ImgLocation)
        .pipe(imagemin())
        .pipe(gulp.dest(ImgDestination))
);


//Bower_components css task
gulp.task('bower-css', function() {
  var cssFilter = gulpFilter( ['**/*.scss', '**/*.css']);
  return gulp.src(mainBowerFiles(),  { base: './bower_components' })
    .pipe(cssFilter)
    .pipe(sass({outputStyle: 'compressed'}))
    .pipe(concat('vendor.css'))
    .pipe(gulp.dest(SassToCssLocation))   
})

gulp.task('bower-js', function() {
  var jsFilter = gulpFilter('**/*.js')
  return gulp.src(mainBowerFiles(),  { base: './bower_components' })
    .pipe(jsFilter)
    .pipe(uglify())
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest(JsDestination))    
})

//Watch task
gulp.task('default',['browser-sync', 'styles', 'bower-css', 'bower-js', 'scripts', 'optimize-image'], function() {
	gulp.watch( projectPHPWatchFiles, reload ); // Reload on PHP file changes.
    gulp.watch(watchSass,['styles', reload]);
    gulp.watch(watchJS,['scripts', reload]);
});